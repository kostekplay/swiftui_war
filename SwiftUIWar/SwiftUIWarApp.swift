////  SwiftUIWarApp.swift
//  SwiftUIWar
//
//  Created on 20/11/2020.
//  
//

import SwiftUI

@main
struct SwiftUIWarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
